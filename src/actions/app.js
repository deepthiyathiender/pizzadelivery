import { ADD_NEW_PIZZA, READ_NEW_ORDERS, CHECK_PIZZAS } from '../constants/action-types';

export function AddNewPizza(pizza) {
    return {
        type: ADD_NEW_PIZZA,
        pizza: pizza
    }
}

function UpdateOrder(pizza){
    return {
        type: READ_NEW_ORDERS,
        pizza: pizza
    }
}

function ToDelivery(pizza){
    return {
        type: CHECK_PIZZAS,
        pizza: pizza
    }
}
export function ReadFromNewOrders(dispatch, pizza) {
    for (let x = 0; x < pizza.length; x++) {
        setTimeout(function(y) {
            dispatch(UpdateOrder(pizza[y]));
        }, x * 1000, x);
    }
}

export function ArePizzasDone(dispatch, pizza) {
    for (let x = 0; x < pizza.length; x++) {
        setTimeout(function(y) {
            dispatch(ToDelivery(pizza[y]));
        }, x * 1000, x);
    }
}