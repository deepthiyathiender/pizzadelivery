import {ADD_NEW_PIZZA, READ_NEW_ORDERS, CHECK_PIZZAS} from '../constants/action-types';

const initialState = {
    pizza: []
};

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4();
}

const app = (state = initialState, action) => {

    switch (action.type) {
        case ADD_NEW_PIZZA:
            var pizza = [];
            state.pizza.map((item) => {
                pizza.push(item);
            });
            for(let p = 0; p<action.pizza.quantity; p++){
                pizza.push({ ID: guid(), type: action.pizza.type, status: "ordered" });
            }
            return {
                pizza: pizza
            };

        case READ_NEW_ORDERS:
            
            var updatedPizza = [];

            state.pizza.map((item) => {
                if(item.ID == action.pizza.ID){
                    updatedPizza.push({
                        ID: item.ID,
                        type: item.type,
                        status: item.type == 0?"veg":"nonveg",
                        startTime: new Date().getTime()
                    })
                }
                else{
                    updatedPizza.push(item);
                }
            });
            return {
                pizza: updatedPizza
            };

        case CHECK_PIZZAS:
            debugger;

            var now = new Date().getTime();

            var DeliveryPizza = [];

            state.pizza.map((item) => {
                debugger;
                if(item.ID == action.pizza.ID && now - item.startTime > 20){
                    DeliveryPizza.push({
                        ID: item.ID,
                        type: item.type,
                        status: "delivery"
                    })
                }
                else{
                    DeliveryPizza.push(item);
                }
            });
            return {
                pizza: DeliveryPizza
            };


    }
    return state;
};

export default app;