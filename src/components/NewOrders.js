import React, {PropTypes, Component} from 'react';
import styles from './app.css';
import { RED_PIZZA, GREEN_PIZZA } from '../constants/action-types';

export const NewOrders = (props) =>  {

    var pizza = props.pizza;
    return(
        <div>
            <table>
                <tbody>
                    <tr>
                        {
                            pizza.map((item) => {
                                return (
                                    <td>{
                                        item.type==0?<img src={ GREEN_PIZZA } /> : <img src= { RED_PIZZA } />
                                    }
                                    </td>
                                )
                            })
                        }
                    </tr>
                </tbody>
            </table>
        </div>
    )
};

NewOrders.propTypes = {

};

export default NewOrders;