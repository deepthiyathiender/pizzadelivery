import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import styles from './app.css';
import {AddNewPizza, ReadFromNewOrders, ArePizzasDone} from '../actions/app';
import NewOrders from '../components/NewOrders'
import {Button} from 'react-bootstrap';

export class App extends Component {

    static propTypes = {

    };

    constructor(props) {
        super(props);

        this.state = {
            quantity: 1
        }
    }

    render() {
        var Orderedpizza = this.props.pizza.filter((item) => {
            if(item.status == "ordered"){
                return true;
            }
        });

        var vegPizza = this.props.pizza.filter((item) => {if(item.status == "veg") { return true; }});
        var nonVegPizza = this.props.pizza.filter((item) => {if(item.status == "nonveg") { return true; }});
        var Delivered = this.props.pizza.filter((item) => {if(item.status == "delivery") { return true; }});


        return (
            <div>
                <table>
                    <tbody>
                        <tr>
                            <td> Quantity:</td>
                                <td><input type="text" value={ this.state.quantity } name="type"
                                           onChange={ this.onQuantityChange.bind(this) }/></td>
                                <td>Non-Veg: </td><td><input type="radio" name="type" onClick={ this.onNonVegClick.bind(this) }/></td>
                                <td> Veg: </td><td><input type="radio" name="type" onClick={ this.onVegClick.bind(this) }/></td>
                            <td><Button bsStyle="primary" onClick={ this.onClickSubmit.bind(this) }> Submit </Button></td>
                        </tr>
                    </tbody>
                </table>
                <hr />
                <h3> Ordered </h3>
                <NewOrders pizza={ Orderedpizza } />
                <hr />
                <h3> Veg </h3>
                <NewOrders pizza={ vegPizza } />
                <hr />
                <h3> Non Veg </h3>
                <NewOrders pizza={ nonVegPizza } />
                <hr />
                <h3> Delivered </h3>
                <NewOrders pizza={ Delivered } />
            </div>
        );
    }

    componentDidMount() {
        const { dispatch } = this.props;
        var that = this;

        //from ordering to veg-non-veg queue
        window.setInterval(() => {
            var Orderedpizza = that.props.pizza.filter((item) => {
                if(item.status == "ordered"){
                    return true;
                }
            });
            dispatch(ReadFromNewOrders(dispatch, Orderedpizza));
        }, 10000);

        //from veg-non-veg to delivery queue
        window.setInterval(() => {
            var DeliveryPizza = that.props.pizza.filter((item) => {
                if(item.status == "veg" || item.status == "nonveg"){
                    return true;
                }
            });
            dispatch(ArePizzasDone(dispatch, DeliveryPizza));
        }, 10000);

    }

    onQuantityChange(event) {
        this.setState ({
            quantity: event.target.value==""?"":parseInt(event.target.value)
        })
    }

    onNonVegClick() {
        this.setState({
            type:1
        })
    }

    onVegClick() {
        this.setState({
            type:0
        })
    }

    onClickSubmit() {
        const { dispatch } = this.props;
        dispatch(AddNewPizza(this.state));
        this.setState({
            quantity: 1
        })
    }
}

function mapStateToProperties(state) {
    return {
        pizza: state.app.pizza
    };
}

export default connect(mapStateToProperties)(App);


